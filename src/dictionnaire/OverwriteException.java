package dictionnaire;

@SuppressWarnings("serial")
public class OverwriteException extends IndexChainListException {
	public OverwriteException(String s) {
		super(s);
	}
}
