package dictionnaire;

@SuppressWarnings("serial")
public class UnpresentException extends IndexChainListException {
	public UnpresentException(String s) {
		super(s);
	}
}
