package dictionnaire;

@SuppressWarnings("serial")
public class IndexPresentException extends IndexChainListException {
	public IndexPresentException(String s) {
		super(s);
	}
}
