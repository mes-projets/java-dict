package dictionnaire;

@SuppressWarnings("serial")
public class IndexChainListException extends Exception {
	public IndexChainListException(String s) {
		super(s);
	}
}
