package dictionnaire;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * IndexChainList permet de stocker des objets et de les identifier par une
 * chaine de caract�re. Ainsi on identifie un objet par sa cl� de type String.
 * 
 * @author Romain
 *
 * @param <T>
 */
public class IndexChainList<T> {

	private T element;
	private String index;
	private IndexChainList<T> fils;
	private boolean raiseExceptionOverwrite;

	/**
	 * Initialise l'IndexChainList
	 */
	public IndexChainList() {
		this.index = "";
		this.raiseExceptionOverwrite = false;
	}

	/**
	 * D�fini si oui ou non, il faut lever une exception lorsque l'on ajoute un
	 * objet ayant un index d�j� enregistrer, ce qui peux �viter l'�crasement de
	 * donn�ees.
	 * 
	 * @param yes
	 */
	public void throwExceptionWhenTryingOverwrite(boolean yes) {
		this.raiseExceptionOverwrite = yes;
	}

	/**
	 * Ajoute un �l�ment associ� � son index.
	 * 
	 * @param index
	 * @param element
	 * @throws IndexPresentException
	 * @throws OverwriteException
	 */
	public void add(String index, T element) throws IndexPresentException, OverwriteException {
		if (this.index.equals(index)) {
			if (this.raiseExceptionOverwrite)
				throw new OverwriteException("Tentative d'ecrasement de l'index : " + index);
			this.element = element;
		} else {
			if (this.element == null) {
				this.element = element;
				this.index = index;
			} else {
				if (this.fils == null)
					this.fils = new IndexChainList<T>();
				this.fils.add(index, element);
			}
		}
	}

	/**
	 * Retourne l'�l�ment associ� � l'index.
	 * 
	 * @param index
	 * @return
	 * @throws UnpresentException
	 */
	public T get(String index) throws UnpresentException {
		if (this.index.equals(index))
			return this.element;
		if (this.fils == null)
			throw new UnpresentException("L'element d'index " + index + " n'est pas pr�sent !");
		return (T) this.fils.get(index);
	}

	private T remove(String index, int local) {
		if (this.index.equals(index)) {
			T elmt = this.element;
			if (this.fils != null) {
				this.element = this.fils.element;
				this.index = this.fils.index;
				this.fils = this.fils.fils;
			} else {
				this.element = null;
				this.index = "";
			}
			return elmt;
		} else {
			return this.fils.remove(index, 1);
		}
	}

	/**
	 * Retourne et supprimme l'entr�e associ� � l'index (et l'index lui-m�me)
	 * 
	 * @param index
	 * @param local
	 * @return
	 */
	public T pop(String index) {
		return this.remove(index, 1);
	}

	/**
	 * Supprimme l'entr�e associ� � l'index (et l'index lui-m�me)
	 * 
	 * @param index
	 * @param local
	 * @return
	 */
	public void remove(String index) {
		this.remove(index, 1);
	}

	/**
	 * Retourne la liste des Index
	 * 
	 * @return
	 */
	public List<String> getIndexList() {
		ArrayList<String> al = new ArrayList<>();
		al.add(index);
		if (this.fils != null)
			al.addAll(this.fils.getIndexList());
		return al;
	}

	/**
	 * Retourne la liste des elements contenu dans la chaine.
	 * 
	 * @return
	 */
	public List<T> getElementList() {
		ArrayList<T> al = new ArrayList<>();
		al.add(this.element);
		if (this.fils != null)
			al.addAll(this.fils.getElementList());
		return al;
	}

	/**
	 * Renvoi le contenu de l'IndexChainList sous forme de JSON
	 */
	public String toString() {
		if (this.fils == null) {
			if (this.element == null)
				return "{}";
			return "{'" + index + "': " + element + "}";
		}
		return "{'" + index + "': " + element.toString() + ", fils: " + this.fils.toString() + "}";
	}

}
