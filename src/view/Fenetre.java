package view;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import dictionnaire.IndexChainList;
import dictionnaire.IndexChainListException;

/**
 * Fenetre est une classe qui genere une fenetre et permet d'ajouter des
 * elements Swing facilement et rapidement. Il utilise la classe IndexChainList
 * afin de stocker les elements.
 * 
 * @author Romain
 *
 */
public class Fenetre {
	public final static int MAIN = 1;

	private JFrame frame;
	private GridLayout layout;

	private IndexChainList<JButton> bouton;
	private IndexChainList<JLabel> label;
	private IndexChainList<JTextField> textField;
	private IndexChainList<JTable> tableau;
	private IndexChainList<JPanel> panneau;
	@SuppressWarnings("rawtypes")
	private IndexChainList<JComboBox> comboBox;

	private IndexChainList<JSpinner> spinner;

	/**
	 * Genere une fenetre
	 * 
	 * @param title
	 */
	public Fenetre(String title) {
		this(title, 0);
	}

	/**
	 * Genere une fenetre, en passant le parametre MAIN en argument, la fenetre
	 * coupera le programme � sa fermeture.
	 * 
	 * @param title
	 * @param arg
	 */
	public Fenetre(String title, int arg) {
		initIndexChainList();
		initGUI(title, arg);
	}

	private void initIndexChainList() {
		bouton = new IndexChainList<>();
		label = new IndexChainList<>();
		textField = new IndexChainList<>();
		tableau = new IndexChainList<>();
		panneau = new IndexChainList<>();
		comboBox = new IndexChainList<>();
		spinner = new IndexChainList<>();
	}

	/**
	 * Renvoi la liste des boutons de la fenetre.
	 * 
	 * @return
	 */
	public IndexChainList<JButton> getBouton() {
		return this.bouton;
	}

	/**
	 * Renvoi la liste des label de la fenetre.
	 * 
	 * @return
	 */
	public IndexChainList<JLabel> getLabel() {
		return this.label;
	}

	/**
	 * Renvoi la liste des textField de la fenetre.
	 * 
	 * @return
	 */
	public IndexChainList<JTextField> getTextField() {
		return this.textField;
	}

	/**
	 * Renvoi la liste des tableaux de la fenetre.
	 * 
	 * @return
	 */
	public IndexChainList<JTable> getTableau() {
		return this.tableau;
	}

	/**
	 * Renvoi la liste des panneaux de la fenetre.
	 * 
	 * @return
	 */
	public IndexChainList<JPanel> getPanneau() {
		return this.panneau;
	}

	/**
	 * Renvoi la liste des comboBox de la fenetre.
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public IndexChainList<JComboBox> getComboBox() {
		return this.comboBox;
	}

	public IndexChainList<JSpinner> getSpinner() {
		return this.spinner;
	}

	private void initGUI(String title, int arg) {
		frame = new JFrame(title);

		if (arg == Fenetre.MAIN)
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		frame.pack();
		display();
	}

	private void refresh() {
		frame.pack();
	}

	/**
	 * Rend la fenetre visible.
	 */
	public void display() {
		frame.setVisible(true);
	}

	/**
	 * Rend la fenetre invisible.
	 */
	public void hide() {
		frame.setVisible(false);
	}

	/**
	 * Affecte un gridLayout � la fenetre.
	 * 
	 * @param colonne
	 * @param ligne
	 */
	public void setGridLayout(int colonne, int ligne) {
		layout = new GridLayout(colonne, ligne);
		frame.setLayout(layout);
	}

	/**
	 * Affecte un gridLayout au panneau d'index pass� en param�tre.
	 * 
	 * @param colonne
	 * @param ligne
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void setGridLayoutToPanel(int colonne, int ligne, String idPanel) throws IndexChainListException {
		this.panneau.get(idPanel).setLayout(new GridLayout(colonne, ligne));
	}

	/**
	 * Ajoute un bouton � la fenetre. Si l'index n'est pas sp�cifi�, le text fourni
	 * sera utiliser.
	 * 
	 * @param text
	 * @throws IndexChainListException
	 */
	public void addButton(String text) throws IndexChainListException {
		this.bouton.add(text, new JButton(text));
		frame.add(this.bouton.get(text));
		refresh();
	}

	/**
	 * Ajoute un bouton � la fenetre
	 * 
	 * @param id
	 * @param text
	 * @throws IndexChainListException
	 */
	public void addButton(String id, String text) throws IndexChainListException {
		this.bouton.add(id, new JButton(text));
		frame.add(this.bouton.get(id));
		refresh();
	}

	/**
	 * Ajoute un bouton � la fenetre et lui affecte un ActionListener.
	 * 
	 * @param id
	 * @param text
	 * @param al
	 * @throws IndexChainListException
	 */
	public void addButton(String id, String text, ActionListener al) throws IndexChainListException {
		this.bouton.add(id, new JButton(text));
		JButton b = this.bouton.get(id);
		b.addActionListener(al);
		frame.add(b);
		refresh();
	}

	/**
	 * Ajoute un bouton au panneau
	 * 
	 * @param text
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addButtonToPanel(String text, String idPanel) throws IndexChainListException {
		this.bouton.add(text, new JButton(text));
		this.panneau.get(idPanel).add(this.bouton.get(text));
		refresh();
	}

	/**
	 * Ajoute un bouton au panneau. Si l'index n'est pas sp�cifi�, le text fourni
	 * sera utiliser.
	 * 
	 * @param id
	 * @param text
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addButtonToPanel(String id, String text, String idPanel) throws IndexChainListException {
		this.bouton.add(id, new JButton(text));
		this.panneau.get(idPanel).add(this.bouton.get(id));
		refresh();
	}

	/**
	 * Ajoute un bouton au panneau et lui affecte un ActionListener.
	 * 
	 * @param id
	 * @param text
	 * @param al
	 * @throws IndexChainListException
	 */
	public void addButtonToPanel(String id, String text, ActionListener al, String idPanel)
			throws IndexChainListException {
		this.bouton.add(id, new JButton(text));
		JButton b = this.bouton.get(id);
		b.addActionListener(al);
		this.panneau.get(idPanel).add(b);
		refresh();
	}

	/**
	 * Supprime le bouton
	 * 
	 * @param index
	 */
	public void removeButton(String index) {
		frame.remove(bouton.pop(index));
	}

	/**
	 * Ajoute un action listener sur le bouton.
	 * 
	 * @param id
	 * @param al
	 * @throws IndexChainListException
	 */
	public void addActionListenerOnButton(String id, ActionListener al) throws IndexChainListException {
		this.bouton.get(id).addActionListener(al);
	}

	/**
	 * Ajoute un label a la fenetre. Si l'index n'est pas sp�cifi�, le text fourni
	 * sera utiliser.
	 * 
	 * @param text
	 * @throws IndexChainListException
	 */
	public void addLabel(String text) throws IndexChainListException {
		this.label.add(text, new JLabel(text));
		frame.add(this.label.get(text));
		refresh();
	}

	/**
	 * Ajoute un label a la fenetre.
	 * 
	 * @param id
	 * @param text
	 * @throws IndexChainListException
	 */
	public void addLabel(String id, String text) throws IndexChainListException {
		this.label.add(id, new JLabel(text));
		frame.add(this.label.get(id));
		refresh();
	}

	/**
	 * Ajoute un label au panneau. Si l'index n'est pas sp�cifi�, le text fourni
	 * sera utiliser.
	 * 
	 * @param text
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addLabelToPanel(String text, String idPanel) throws IndexChainListException {
		this.label.add(text, new JLabel(text));
		this.panneau.get(idPanel).add(this.label.get(text));
		refresh();
	}

	/**
	 * Ajoute un label au panneau.
	 * 
	 * @param id
	 * @param text
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addLabelToPanel(String id, String text, String idPanel) throws IndexChainListException {
		this.label.add(id, new JLabel(text));
		this.panneau.get(idPanel).add(this.label.get(id));
		refresh();
	}

	/**
	 * Supprime le label.
	 * 
	 * @param index
	 */
	public void removeLabel(String index) {
		frame.remove(label.pop(index));
	}

	/**
	 * Ajoute un label vide � la fenetre afin de remplir une case du gridLayout. Si
	 * l'index n'est pas sp�cifi�, il sera mis � _white_blank_space_.
	 * 
	 * @throws IndexChainListException
	 */
	public void addBlank() throws IndexChainListException {
		this.addLabel("_white_blank_space_", "");
	}

	/**
	 * Ajoute un label vide � la fenetre afin de remplir une case du gridLayout.
	 * 
	 * @param id
	 * @throws IndexChainListException
	 */
	public void addBlank(String id) throws IndexChainListException {
		this.addLabel(id, "");
	}

	/**
	 * Ajoute un label vide au panneau afin de remplir une case du gridLayout. Si
	 * l'index n'est pas sp�cifi�, il sera mis � _white_blank_space_.
	 * 
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addBlankToPanel(String idPanel) throws IndexChainListException {
		this.addLabelToPanel("_white_blank_space_", "", idPanel);
	}

	/**
	 * Ajoute un label vide au panneau afin de remplir une case du gridLayout.
	 * 
	 * @throws IndexChainListException
	 */
	public void addBlankToPanel(String id, String idPanel) throws IndexChainListException {
		this.addLabelToPanel(id, "", idPanel);
	}

	/**
	 * Ajoute un champs de texte � la fenetre.
	 * 
	 * @param index
	 * @throws IndexChainListException
	 */
	public void addTextField(String index) throws IndexChainListException {
		this.textField.add(index, new JTextField());
		frame.add(this.textField.get(index));
		refresh();
	}

	/**
	 * Ajoute un champs de texte � la fenetre avec du texte pr�-rempli.
	 * 
	 * @param index
	 * @param text
	 * @throws IndexChainListException
	 */
	public void addTextField(String index, String text) throws IndexChainListException {
		this.textField.add(index, new JTextField(text));
		frame.add(this.textField.get(index));
		refresh();
	}

	/**
	 * Ajoute un champs de texte au panneau.
	 * 
	 * @param index
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addTextFieldToPanel(String index, String idPanel) throws IndexChainListException {
		this.textField.add(index, new JTextField());
		this.panneau.get(idPanel).add(this.textField.get(index));
		refresh();
	}

	/**
	 * Ajoute un champs de texte au panneau avec du texte pr�-rempli.
	 * 
	 * @param index
	 * @param text
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addTextFieldToPanel(String index, String text, String idPanel) throws IndexChainListException {
		this.textField.add(index, new JTextField(text));
		this.panneau.get(idPanel).add(this.textField.get(index));
		refresh();
	}

	/**
	 * Supprime le champs de texte.
	 * 
	 * @param index
	 */
	public void removeTextField(String index) {
		frame.remove(textField.pop(index));
	}

	/**
	 * Ajoute un tableau � la fenetre.
	 * 
	 * @param index
	 * @param data
	 * @param entete
	 * @throws IndexChainListException
	 */
	public void addTableau(String index, Object[][] data, String[] entete) throws IndexChainListException {
		this.tableau.add(index, new JTable(data, entete));
		JTable t = this.tableau.get(index);
		frame.add(t.getTableHeader());
		frame.add(t);
		refresh();
	}

	/**
	 * Ajoute un tableau au panneau.
	 * 
	 * @param index
	 * @param data
	 * @param entete
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addTableauToPanel(String index, Object[][] data, String[] entete, String idPanel)
			throws IndexChainListException {
		this.tableau.add(index, new JTable(data, entete));
		JTable t = this.tableau.get(index);
		this.panneau.get(idPanel).add(t.getTableHeader());
		this.panneau.get(idPanel).add(t);
		refresh();
	}

	/**
	 * Supprimme le tableau.
	 * 
	 * @param index
	 */
	public void removeTableau(String index) {
		frame.remove(tableau.pop(index));
	}

	/**
	 * Ajoute un panneau � la fenetre.
	 * 
	 * @param index
	 * @throws IndexChainListException
	 */
	public void addPanel(String index) throws IndexChainListException {
		this.panneau.add(index, new JPanel());
		frame.add(this.panneau.get(index));
		refresh();
	}

	/**
	 * Ajoute un panneau au panneau.
	 * 
	 * @param index
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addPanelToPanel(String index, String idPanel) throws IndexChainListException {
		this.panneau.add(index, new JPanel());
		this.panneau.get(idPanel).add(this.panneau.get(index));
		refresh();
	}

	/**
	 * Supprime un panneau.
	 * 
	 * @param index
	 */
	public void removePanneau(String index) {
		frame.remove(panneau.pop(index));
	}

	/**
	 * Ajoute une comboBox � la fenetre.
	 * 
	 * @param index
	 * @param elmt
	 * @throws IndexChainListException
	 */
	public void addComboBox(String index, String[] elmt) throws IndexChainListException {
		addComboBox(index, elmt, 0);
	}

	/**
	 * Ajoute une comboBox � la fenetre en sp�cifiant l'�l�ment choisi � sa
	 * cr�ation.
	 * 
	 * @param index
	 * @param elmt
	 * @param base
	 * @throws IndexChainListException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addComboBox(String index, String[] elmt, int base) throws IndexChainListException {
		JComboBox jcb = new JComboBox(elmt);
		jcb.setSelectedIndex(base);
		this.comboBox.add(index, jcb);
		this.frame.add(jcb);
	}

	/**
	 * Ajoute un comboBox au panneau.
	 * 
	 * @param index
	 * @param elmt
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	public void addComboBoxToPanel(String index, String[] elmt, String idPanel) throws IndexChainListException {
		addComboBoxToPanel(index, elmt, 0, idPanel);
	}

	/**
	 * Ajoute un comboBox au panneau en sp�cifiant l'�l�ment choisi � sa cr�ation.
	 * 
	 * @param index
	 * @param elmt
	 * @param base
	 * @param idPanel
	 * @throws IndexChainListException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addComboBoxToPanel(String index, String[] elmt, int base, String idPanel)
			throws IndexChainListException {
		JComboBox jcb = new JComboBox(elmt);
		jcb.setSelectedIndex(base);
		this.comboBox.add(index, jcb);
		this.panneau.get(idPanel).add(jcb);
	}

	public void addIntSpinner(String index, int min, int max, int step) throws IndexChainListException {
		addIntSpinner(index, min, max, step, false);
	}

	public void addIntSpinner(String index, int min, int max, int step, boolean positive)
			throws IndexChainListException {
		SpinnerNumberModel model = new SpinnerNumberModel(min, min, max, step);
		this.spinner.add(index, new JSpinner());
		this.spinner.get(index).setModel(model);
		frame.add(this.spinner.get(index));
		refresh();
	}

	public void addIntSpinnerToSpinner(String index, int min, int max, int step, String idPanel)
			throws IndexChainListException {
		addIntSpinnerToSpinner(index, min, max, step, false, idPanel);
	}

	public void addIntSpinnerToSpinner(String index, int min, int max, int step, boolean positive, String idPanel)
			throws IndexChainListException {
		SpinnerNumberModel model = new SpinnerNumberModel(min, min, max, step);
		this.spinner.add(index, new JSpinner());
		this.spinner.get(index).setModel(model);
		this.panneau.get(idPanel).add(this.spinner.get(index));
		refresh();
	}

	public JFrame getFrame() {
		return this.frame;
	}
}
