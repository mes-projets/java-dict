package view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import dictionnaire.IndexChainListException;
import dictionnaire.UnpresentException;
import view.Fenetre;

public class Vue implements ActionListener {
	private Fenetre fenetre;

	public Vue() throws IndexChainListException {
		fenetre = new Fenetre("Amortissement", Fenetre.MAIN);
		initFenetre();
	}

	private void initFenetre() throws IndexChainListException {
		fenetre.setGridLayout(0, 1);

		fenetre.addPanel("p1");
		fenetre.setGridLayoutToPanel(0, 4, "p1");
		fenetre.addPanel("p2");
		fenetre.setGridLayoutToPanel(0, 4, "p2");

		fenetre.addLabelToPanel("_white_blank_space_", "", "p1");
		fenetre.addLabelToPanel("titre", "Calcul amortissement", "p1");
		fenetre.addLabelToPanel("_white_blank_space_", "", "p1");
		fenetre.addLabelToPanel("_white_blank_space_", "", "p1");
		fenetre.addLabelToPanel("designation", "Designation :", "p1");
		fenetre.addTextFieldToPanel("designation", "p1");

		fenetre.addLabelToPanel("duree", "Dur�e d'amortissement :", "p2");
		fenetre.addIntSpinnerToSpinner("duree", 3, 50, 1, true, "p2");
		fenetre.addLabelToPanel("type_amortissement", "Type d'amortissement :", "p2");
		String[] choix = { "Lin�aire", "Economique", "D�gressif" };
		fenetre.addComboBoxToPanel("type_amortissement", choix, "p2");

		fenetre.addLabelToPanel("mise_en_service", "Date de mise en service :", "p2");
		fenetre.addLabelToPanel("Ici le choix de la date", "p2");
		fenetre.addLabelToPanel("taux", "Taux d'amortissement :", "p2");
		fenetre.addLabelToPanel("taux", "", "p2");

		fenetre.addLabelToPanel("date_fin", "Date de fin :", "p2");
		fenetre.addLabelToPanel("date_fin", "", "p2");
		fenetre.addLabelToPanel("valeur", "Valeur � amortir :", "p2");
		fenetre.addIntSpinnerToSpinner("valeur", 0, 2147483647, 1, true, "p2");

		fenetre.addBlankToPanel("p2");
		fenetre.addBlankToPanel("p2");
		fenetre.addBlankToPanel("p2");
		fenetre.addButtonToPanel("start_calcul", "Calculer", this, "p2");

	}

	public Fenetre getFenetre() {
		return this.fenetre;
	}

	public void setTableau(Object[][] data, String[] head) throws IndexChainListException {
		try {
			fenetre.getPanneau().get("pTableau");

		} catch (IndexChainListException icle) {
			fenetre.addPanel("pTableau");
		}
		fenetre.getPanneau().get("pTableau").removeAll();
		fenetre.setGridLayoutToPanel(0, 1, "pTableau");
		System.out.println("Creation tableau");
		JTable tableau = new JTable(data, head);
		System.out.println("Creation pane");
		JScrollPane pane = new JScrollPane(tableau);
		fenetre.getPanneau().get("pTableau").add(pane);
		System.out.println("Ok ajout tableau");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
